Google In App Review
------------------------------------------------------------------

Integration Steps
------------------------------------------------------------------
1. Install or upload FG InAppReview and Google Play In App Review plugins from the FunGames Integration Manager.

2. Follow the instructions in the "Install External Plugin" section to import the Google Play In App Review API.

3. Click on the "Prefabs and Settings" button in the FunGames Integration Manager to fill up your scene with the required components and create the Settings asset.

4. To finish your integration, follow the Account and Settings section.

Install External Plugin
------------------------------------------------------------------
After importing the FG InAppReview module from the Integration Manager window, you will find the latest compatible version of the Google Play Review API in the Assets > GooglePlayPlugins > com.google.play.review folder.

If you wish to install the newest version of the API, you can download it from the Google Play Developer site.

Note that the versions of included external APIs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepancies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.

Configure Project Settings
------------------------------------------------------------------
1.Open your project in Unity.

2. Go to Edit > Project Settings > Player.

3. In the Android tab, make sure you have set up your bundle identifier, version, and other necessary settings.

4. Ensure that your project is configured to support the required API levels for Google Play services.

Then, select the FGGoogleInAppReviewSettings asset in the Resources folder (Assets > Resources > FunGames > FGGoogleInAppReviewSettings).

To test the integration, please first run your game by building and running it on your Android device as the review prompt only works on mobile. Once the game is launched, trigger the in app review process to ensure everything is working correctly.
